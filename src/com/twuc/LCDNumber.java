package com.twuc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LCDNumber implements LCDNumberIntrface {

    private int w;
    private int h;
    private char c;
    private List<List<Integer>> data;

    LCDNumber(int w, int h, char c) {
        this.w = w;
        this.h = h;
        this.c = c;
    }

    @Override
    public void init() {
        data  = new ArrayList<>();
        for(int i = 0; i< this.getH(); i++){
            List<Integer> line = new ArrayList<>();
            for(int j = 0;j<this.getW();j++){
                line.add(0);
            }
            data.add(line);
        }

        Integer[] line0 = new Integer[3];data.get(0).toArray(line0);
        Integer[] line1 = new Integer[3];data.get(1).toArray(line1);
        Integer[] line2 = new Integer[3];data.get(2).toArray(line2);
        Integer[] line3 = new Integer[3];data.get(3).toArray(line3);
        Integer[] line4 = new Integer[3];data.get(4).toArray(line4);

        switch (c){
            case '0':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 0;  line2[2] = 0;
                line3[0] = 1;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '1':
                line0[0] = 0;  line0[1] = 0;  line0[2] = 0;
                line1[0] = 0;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 0;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 0;  line4[2] = 0;
                break;
            case '2':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 0;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 1;  line3[1] = 0;  line3[2] = 0;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '3':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 0;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '4':
                line0[0] = 0;  line0[1] = 0;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 0;  line4[2] = 0;
                break;
            case '5':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 0;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '6':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 0;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 1;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '7':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 0;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 0;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 0;  line4[2] = 0;
                break;
            case '8':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 1;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
            case '9':
                line0[0] = 0;  line0[1] = 2;  line0[2] = 0;
                line1[0] = 1;  line1[1] = 0;  line1[2] = 1;
                line2[0] = 0;  line2[1] = 2;  line2[2] = 0;
                line3[0] = 0;  line3[1] = 0;  line3[2] = 1;
                line4[0] = 0;  line4[1] = 2;  line4[2] = 0;
                break;
        }

        data.clear();
        data.add(Arrays.asList(line0));
        data.add(Arrays.asList(line1));
        data.add(Arrays.asList(line2));
        data.add(Arrays.asList(line3));
        data.add(Arrays.asList(line4));
    }

    @Override
    public void print() {
        for(int i = 0; i< this.getH(); i++){
            for(int j = 0;j<this.getW();j++){
                System.out.print(data.get(i).get(j));
            }
            System.out.print("\n");
        }
    }

    @Override
    public void scale(int size) {
        this.w = (this.w-2) * size + 2;
        this.h = (this.h-3) * size + 3;

        Integer[][] newNum = new Integer[this.h][this.w];

        for(int i = 0; i< this.getH(); i++){
            for(int j = 0;j<this.getW();j++){
                newNum[i][j]=3;
            }
        }

        for(int i = 0; i< (this.getH()-3)/size+3; i++){
            for(int j = 0;j<(this.getW()-2)/size+2;j++){
                switch (data.get(i).get(j)){
                    case 1:
                        if(i==1 && j==0){
                            newNum[i][j] = 1;
                        }
                        if(i==3 && j==0){
                            newNum[i + size - 1][j] = 1;
                        }
                        if(i==1 && j==2){
                            newNum[i][j + size - 1] = 1;
                        }
                        if(i==3 && j==2){
                            newNum[i + size - 1][j + size - 1] = 1;
                        }
                        break;
                    case 2:
                        if(i==0) {
                            newNum[i][j] = 2;
                        }
                        if(i==2) {
                            newNum[i + size - 1][j] = 2;
                        }
                        if(i==4) {
                            newNum[this.h-1][j] = 2;
                        }
                        break;
                }
            }
        }

        Integer[][] expandNum = new Integer[this.h][this.w];

        for(int i = 0; i< this.getH(); i++){
            for(int j = 0;j<this.getW();j++){
                expandNum[i][j]=0;
            }
        }

        for(int i = 0; i< this.getH(); i++){
            for(int j = 0;j<this.getW();j++){
                switch (newNum[i][j]){
                    case 1:
                        for(int t = 0;t<size;t++) {
                            expandNum[i + t][j] = 1;
                        }
                        break;
                    case 2:
                        for(int t = 0;t<size;t++) {
                            expandNum[i][j + t] = 2;
                        }
                        break;
                }
            }
        }

        data.clear();
        for(Integer[] str : expandNum){
            data.add(Arrays.asList(str));
        }
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public List<List<Integer>> getData() {
        return data;
    }

    public void setData(List<List<Integer>> data) {
        this.data = data;
    }
}

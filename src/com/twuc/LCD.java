package com.twuc;

import java.util.ArrayList;
import java.util.List;

class LCD {

    private List<LCDNumber> numberList;
    private int w = 3;
    private int h = 5;
    private String nums;

    LCD(String nums) {
        numberList = new ArrayList<>();
        this.nums = nums;
        for (char c : nums.toCharArray()) {
            LCDNumber lcdNumber = new LCDNumber(w, h, c);
            lcdNumber.init();
            numberList.add(lcdNumber);
        }
    }

    void scale(int size){
        this.w = (this.w-2) * size + 2;
        this.h = (this.h-3) * size + 3;
        for (LCDNumber lcdNumber : numberList) {
            lcdNumber.scale(size);
        }
    }

    void print() {
        for (int i = 0; i < this.getH(); i++) {
            for (LCDNumber lcdNumber : numberList) {
                for (int j = 0; j < this.getW(); j++) {
                    switch (lcdNumber.getData().get(i).get(j)){
                        case 0:
                            System.out.print(" ");
                            break;
                        case 1:
                            System.out.print("|");
                            break;
                        case 2:
                            System.out.print("-");
                            break;

                    }

                }
                System.out.print(" ");
            }
            System.out.print("\n");
        }
    }


    public List<LCDNumber> getNumberList() {
        return numberList;
    }

    public void setNumberList(List<LCDNumber> numberList) {
        this.numberList = numberList;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public String getNums() {
        return nums;
    }

    public void setNums(String nums) {
        this.nums = nums;
    }
}

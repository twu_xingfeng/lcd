package com.twuc;

public interface LCDNumberIntrface {

    void init();

    void print();

    void scale(int size);
}
